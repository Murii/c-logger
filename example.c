#include "logger.h"

int main() {
	log_info("Starting! %s", "123");
	log_trace("Trace!");
    log_debug("Debug!");
    log_warning("This is a warning!");
	log_error("This shoudn't have happened!");
	log_fatal("FATAL!");
	return 0;
}
