#include "logger.h"


#define ANSI_COLOR_RESET   "\x1b[0m"
#define ANSI_COLOR_GREY    "\x1b[90m"

static const char *level_colors[] = {
  "\x1b[94m", "\x1b[36m", "\x1b[32m", "\x1b[33m", "\x1b[31m", "\x1b[35m"
};

static const char *level_names[] = {
    "TRACE", "DEBUG", "INFO", "WARNING", "ERROR", "FATAL"
};

void _logger(int level, char *file, int line, char* format, ...) {

	time_t t = time(NULL);
    struct tm *lt = localtime(&t);

    char buf[16];
    buf[strftime(buf, sizeof(buf), "%H:%M:%S", lt)] = '\0';
	#ifdef LOG_USE_COLOR
    	fprintf(stderr, "%s%s %s %s %s %s:%d:%s ",ANSI_COLOR_GREY, buf,
		level_colors[level], level_names[level], ANSI_COLOR_GREY, file, line,
		ANSI_COLOR_RESET);
	#else
		fprintf(stderr, "%s %s %s:%d ", buf, level_names[level], file, line);
	#endif

	va_list args;

	va_start(args, format);
	vprintf(format, args);
	va_end(args);
	printf("\n");
	fflush(stderr);
}
